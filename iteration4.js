// **Iteración #4: Probando For...in**
// Usa un **for...in** para imprimir por consola los datos del alienígena.. Puedes usar este objeto:

const alien = {
    name: 'Wormuck',
    race: 'Cucusumusu',
    planet: 'Eden',
    weight: '259kg'
}


for (var name in alien) {
    console.log(`Soy un alien: `);
    for(var data in alien) {
        console.log(`y mi ${data} es ${alien[data]}`)
    }
    break;
}